from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import requests


def get_location_picture(city):
    url = f"https://api.pexels.com/v1/search?query={city}"
    headers = {"Authorization": PEXELS_API_KEY}
    r = requests.get(url, headers=headers)
    pexel_content = r.json()
    return pexel_content["photos"][0]["url"]


def get_weather_data(city, state):
    result = {}
    try:
        geo_url = f"http://api.openweathermap.org/geo/1.0/direct?q={city},{state},US&appid={OPEN_WEATHER_API_KEY}"
        geo_r = requests.get(geo_url).json()
        lat = geo_r[0]["lat"]
        lon = geo_r[0]["lon"]
        weather_url = f"https://api.openweathermap.org/data/2.5/weather?lat={lat}&lon={lon}&appid={OPEN_WEATHER_API_KEY}"
        weather_r = requests.get(weather_url).json()
        result["temp"] = convert_temp_KtoF(weather_r["main"]["temp"])
        result["description"] = weather_r["weather"][0][
            "description"
        ]
    except:
        result = None
    return result


def convert_temp_KtoF(temp):
    return round(9 / 5 * (temp - 273.15) + 32)
